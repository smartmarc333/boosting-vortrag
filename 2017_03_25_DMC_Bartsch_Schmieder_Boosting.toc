\beamer@endinputifotherversion {3.33pt}
\select@language {ngerman}
\beamer@sectionintoc {1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{AdaBoost Zweiklassenfall}{4}{0}{2}
\beamer@sectionintoc {3}{Forward Stagewise Modeling}{10}{0}{3}
\beamer@sectionintoc {4}{AdaBoost}{11}{0}{4}
\beamer@sectionintoc {5}{Best of the shelf classifier}{12}{0}{5}
\beamer@sectionintoc {6}{Boosting Klassifikationsverfahren in mlr}{14}{0}{6}
\beamer@sectionintoc {7}{Boosting Vorteile}{15}{0}{7}
\beamer@sectionintoc {8}{Boosting Nachteile}{16}{0}{8}
\beamer@sectionintoc {9}{Quellen}{17}{0}{9}
